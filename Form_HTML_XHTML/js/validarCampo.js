
function validarCampos() {
	
	var nome = document.getElementById('nome');
	var cpf = document.getElementById('cpf');
	var email = document.getElementById('email');
	var cep = document.getElementById('cep');
	var numero = document.getElementById('numero');

	if (nome.value == "" || nome.value.length <= 3) {
		alert("Insira um nome com mais de 3 letras!");
		document.getElementById('nome').value = "";
		document.getElementById('nome').focus();
		return false;
	} else {
		var verCPF = validarCPF(cpf.value);
		if (!verCPF) {
			return false;
		} else {
			var verEmail = validarEmail(email.value);
			if (!verEmail) {
				return false;
			} else {
				if (cep.value == "" || cep.value.length < 8) {
					alert("Informe um cep válido!");
					document.getElementById('cep').value = "";
					document.getElementById('cep').focus();
					return false;
				} else {
					if (numero.value == "") {
						alert("Informe o número da casa");
						document.getElementById('numero').value = "";
						document.getElementById('numero').focus();
						return false;
					}
				}
			}
		}
	}
}
