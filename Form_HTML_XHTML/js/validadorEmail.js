function validarEmail(email) {
    if (email == "") {
        alert("Informe o email");
        document.getElementById('email').value = "";
		document.getElementById('email').focus();
        return false;
    }
    var er = new RegExp(/^[A-Za-z0-9_\-\.]+@[A-Za-z0-9_\-\.]{2,}\.[A-Za-z0-9]{2,}(\.[A-Za-z0-9])?/);
    if (typeof (email) == "string") {
        if (er.test(email)) { return true; }
        alert("email invalido");
        document.getElementById('email').value = "";
		document.getElementById('email').focus();
        return false; 
    } else if (typeof (email) == "object") {
        if (er.test(email.value)) {
            return true;
        }
    } else {
        return false;
    }
}